import '../styles/globals.scss'
// import 'tailwindcss/tailwind.css'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import { NextPage } from 'next'
import ConfigureStore from '../store'

const MyApp: NextPage<AppProps> = ({ Component, pageProps }: AppProps) => {
  return (
    <Provider store={ConfigureStore.store}>
      <Component {...pageProps} />
    </Provider>
  )
}
export default MyApp
