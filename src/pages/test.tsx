const TestPage = () => {
  return (
    <div className="h-screen w-full flex justify-center bg-[#0e0]">
      <p className="text-2xl md:text-1xl">Test</p>
    </div>
  )
}

export default TestPage
