import { applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from '../sagas'
import ConfigureStore from './configureStore'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [sagaMiddleware]

const middleWare = applyMiddleware(...middlewares)

const store = ConfigureStore(middleWare)

export default store

sagaMiddleware.run(rootSaga)
