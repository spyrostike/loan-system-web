export interface ITestReducerState {
  text: string
}

export interface ITestReducerPayload {
  type: string
  payload: { key: string; value: string }
}
