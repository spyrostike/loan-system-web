import { ITestReducerState } from './test.interface'

export interface IStore {
  TestReducer: ITestReducerState
}
