import React, { Children, ReactNode } from 'react'
import tw from 'tailwind-styled-components'

export interface Props {
  color?: 'primary' | 'secondary' | 'danger'
  children?: ReactNode
  disabled?: boolean
  onClick?(): void
  style?: React.CSSProperties
}

const Primary = tw.button`
  w-full
  py-2
  rounded-md
  bg-blue-600
  text-gray-100
  focus:outline-none
`

export const Secondary = tw(Primary)`
  bg-green-500
`

export const Danger = tw(Primary)`
  bg-red-600
`

const IButton = ({ color, children, disabled, onClick, style }: Props) => {
  let Button = Primary

  if (color === 'secondary') Button = Secondary
  if (color === 'danger') Button = Danger

  return (
    <Button onClick={onClick} style={style} disabled={disabled}>
      {Children.toArray(children)}
    </Button>
  )
}

export default IButton
