import React from 'react'
import tw from 'tailwind-styled-components'
import { ITMedium } from '..'

export interface Props {
  disabled?: boolean
  errorMessage?: string
  leftIcon?: string
  placeholder?: string
  rightIcon?: string
  onLeftIconClick?: () => void
  onRightIconClick?: () => void
  style?: React.CSSProperties
}

const Container = tw.div`
  w-full
  mb-2
  relative
  flex
  w-full
  flex-wrap
  items-stretch
  mb-3
`

const IconContainer = tw.span`
  z-10
  h-full
  leading-snug
  font-normal
  absolute
  text-center
  ${(props: { errormessage?: string }) =>
    props.errormessage ? 'text-red-500' : 'text-gray-400'}
  absolute
  bg-transparent
  rounded
  text-base
  items-center
  justify-center
  w-8
  pl-3
  py-3
`

const RightIconContainer = tw(IconContainer)`
  right-2
  pr-3
  py-3
`

// const Icon = styled((props) => <i className={`fa ${props.icon}`} />)``

const Icon = tw.i`
  fa
  ${(props: { icon?: string }) => props.icon || ''}
`

const Input = tw.input`
  px-3
  py-3
  placeholder-blueGray-300
  text-blueGray-600
  relative
  bg-white
  rounded
  text-sm
  ${(props: { errormessage?: string }) =>
    props.errormessage
      ? ' border border-red-500 text-red-600'
      : 'border-0 shadow text-blueGray-600'}
  shadow
  outline-none
  focus:outline-none
  focus:ring
  w-full
  pl-10
  pr-10
`

const ITextInput = ({
  disabled,
  errorMessage,
  leftIcon,
  placeholder,
  onLeftIconClick,
  onRightIconClick,
  rightIcon,
  style,
}: Props) => {
  return (
    <>
      <Container>
        {leftIcon && (
          <IconContainer
            errormessage={errorMessage}
            onClick={onLeftIconClick}
            style={{ cursor: onLeftIconClick ? 'pointer' : 'default' }}
          >
            <Icon icon={leftIcon} />
          </IconContainer>
        )}

        <Input
          disabled={disabled}
          errormessage={errorMessage}
          placeholder={placeholder}
          style={style}
        />

        {rightIcon && (
          <RightIconContainer
            errormessage={errorMessage}
            onClick={onRightIconClick}
            style={{ cursor: onRightIconClick ? 'pointer' : 'default' }}
          >
            <Icon icon={rightIcon} />
          </RightIconContainer>
        )}
      </Container>
      <ITMedium color={'red'}>{errorMessage}</ITMedium>
    </>
  )
}

export default ITextInput
