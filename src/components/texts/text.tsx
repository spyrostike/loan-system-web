import tw from 'tailwind-styled-components'

export interface Props {
  color?: string
  fontSize?: 'xl' | 'lg' | 'md' | 'sm' | 'xs'
  class?: string
}

export const IText = tw.p<Props>`
  font-serif
  m-2
`

export const ITMedium = tw(IText)`
  font-medium
  ${(p) => p.class || null}
  ${(p) => (p.color ? `text-${p.color}-500` : 'text-gray-500')}
  ${(p) => (p.fontSize ? `text-${p.fontSize}` : 'text-md')}
`

export const ITLight = tw(IText)<Props>`
  font-extralight
  ${(p) => p.class || null}
  ${(p) => (p.color ? `text-${p.color}-500` : 'text-gray-500')}
  ${(p) => (p.fontSize ? `text-${p.fontSize}` : 'text-md')}
`

export const ITBold = tw(IText)<Props>`
  font-bold
  ${(p) => p.class || null}
  ${(p) => (p.color ? `text-${p.color}-500` : 'text-gray-500')}
  ${(p) => (p.fontSize ? `text-${p.fontSize}` : 'text-md')}
`

export const ITExtraBold = tw(IText)<Props>`
  font-extrabold
  ${(p) => p.class || null}
  ${(p) => (p.color ? `text-${p.color}-500` : 'text-gray-500')}
  ${(p) => (p.fontSize ? `text-${p.fontSize}` : 'text-md')}
`
