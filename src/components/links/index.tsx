import React, { Children, ReactNode } from 'react'
import tw from 'tailwind-styled-components'
import { ITLight } from '../texts'

export interface Props {
  color?: string
  children?: ReactNode
  disabled?: boolean
  style?: React.CSSProperties
  uri?: string
}

const Primary = tw.a`
  text-xs
  float-right
  mb-4
`

export const Secondary = tw(Primary)`
  text-green-500
`

export const Danger = tw(Primary)`
  text-red-600
`

const ILink = ({ color, children, uri, style }: Props) => {
  let A = Primary

  if (color === 'secondary') A = Secondary
  if (color === 'danger') A = Danger

  return (
    <A href={uri} style={style}>
      <ITLight fontSize="xl" color={color}>
        {Children.toArray(children)}
      </ITLight>
    </A>
  )
}

export default ILink
