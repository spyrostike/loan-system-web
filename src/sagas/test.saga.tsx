import { take, call } from 'redux-saga/effects'
import { TEST } from '../actions'

export function* workerTest({ payload }: any) {
  try {
    yield console.log('workerTest payload', payload)
  } catch (error) {
    console.log('error', error)
  }
}

export function* watcherTest() {
  while (true) {
    const action: { type: string; payload: any } = yield take(TEST)
    yield call(workerTest, action)
  }
}
