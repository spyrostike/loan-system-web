import { all, fork } from 'redux-saga/effects'
import { watcherTest } from './test.saga'

export default function* rootSaga() {
  yield all([fork(watcherTest)])
}
