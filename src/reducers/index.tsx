import { combineReducers } from 'redux'
import TestReducer from './test.reducer'

const appReducer = combineReducers({
  TestReducer,
})

const rootReducer = (state: any, action: any) => {
  if (action.type === 'RESET') {
    state = {
      TestReducer: null,
    }
  }

  return appReducer(state, action)
}

export default rootReducer
