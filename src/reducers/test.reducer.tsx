import { TEST_SET_REDUCER } from '../actions'
import { ITestReducerState, ITestReducerPayload } from '../interfaces'

const initialState: ITestReducerState = {
  text: '',
}

function TestReducer(
  state = initialState,
  { type, payload }: ITestReducerPayload
) {
  switch (type) {
    case TEST_SET_REDUCER:
      console.log(TEST_SET_REDUCER, type, payload)
      return { ...state, text: payload }
    default:
      return state
  }
}

export default TestReducer
